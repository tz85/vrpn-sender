
///////////////////// NETWORKING ////////////////////////////////////
/*
** talker.c -- a datagram "client" demo
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <string>
using namespace std;

#define SERVERPORT "49501"    // the port users will be connecting to

int sockfd;
struct addrinfo hints, *servinfo, *p;
bool socket_setup=false;

int setup_socket(string ip_addy)
{
    int rv;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if ((rv = getaddrinfo(ip_addy.c_str(), SERVERPORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and make a socket
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                p->ai_protocol)) == -1) {
            perror("talker: socket");
            continue;
        }

        break;
    }

    if (p == NULL) {
        fprintf(stderr, "talker: failed to create socket\n");
        return 2;
    }    
    else
        printf("setup seems to have worked for UDP socket\n");
    socket_setup=true;


    printf("socket setup for %s was succesfull!\n",ip_addy.c_str());
    return 0;
}

void send_udp(string payload)
{
   int numbytes;

   if(socket_setup==false) 
      return;

   if ((numbytes = sendto(sockfd,payload.c_str(),strlen(payload.c_str()), 0,
             p->ai_addr, p->ai_addrlen)) == -1) {
        perror("send_udp error occured");
        exit(1);
    }
    //printf("sent udp: %s\n",payload.c_str());
}

void send_udp(const unsigned char *payload, int payload_size)
{
   int numbytes;

   if(socket_setup==false) 
      return;

   if ((numbytes = sendto(sockfd,payload,payload_size, 0,
             p->ai_addr, p->ai_addrlen)) == -1) {
        perror("send_udp error occured");
        exit(1);
    }
    //printf("sent udp size: %d\n",numbytes);

}


void close_udp()
{
   if(socket_setup==false) 
      return;

   freeaddrinfo(servinfo);
   close(sockfd);
}
