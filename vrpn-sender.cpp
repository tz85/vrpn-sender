//Borrowed from: https://github.com/vrpn/vrpn/wiki/Client-side-VRPN-Devices

/*
My first vrpn client - vrpnHelloWorld
If you want to transform a CAMERA, VIEWPOINT or HMD, instead of an displayed object,
you need to invert the transform, since
vrpn returns the transform sensor to tracker/base/emitter transform.

// NOTE: a vrpn tracker must call user callbacks with tracker data (pos and
//       ori info) which represent the transformation xfSourceFromSensor.
//       This means that the pos info is the position of the origin of
//       the sensor coord sys in the source coord sys space, and the
//       quat represents the orientation of the sensor relative to the
//       source space (ie, its value rotates the source's axes so that
//       they coincide with the sensor's)
*/

#include <stdlib.h>
#include <stdio.h>
#include <vrpn_Tracker.h>
#include "udp_socket.h"


#include <stdio.h>	/* for printf */
#include <stdint.h>	/* for uint64 definition */
#include <stdlib.h>	/* for exit() definition */
#include <time.h>	/* for clock_gettime */

#define BILLION 1000000000L
#define MILLI   1000L;

#define NUM_SENSORS 1

//https://www.cs.rutgers.edu/~pxk/416/notes/c-tutorials/files/gettime.c

struct timespec start, end;

void start_timer()
{
   clock_gettime(CLOCK_MONOTONIC, &start);	/* mark start time */
}

double end_timer()
{
   uint64_t diff;
   clock_gettime(CLOCK_MONOTONIC, &end);	/* mark the end time */

   diff = BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
   double seconds=(double)diff/(double)BILLION;

   //printf("elapsed time = %f seconds\n", seconds);
   return seconds;
}

/*****************************************************************************
 *
   Callback handler
 *
 *****************************************************************************/

unsigned char send_buffer[NUM_SENSORS*64];
bool sensor_received[NUM_SENSORS];
int frames_sent=2;

bool all_sensors_ready()
{
   for(int i=0;i<NUM_SENSORS;i++)
   {
      if(sensor_received[i]==false)
         return false;
   }

   return true;
}

void clear_all_sensors()
{
   for(int i=0;i<NUM_SENSORS;i++)
      sensor_received[i]=false;
}
	
void    VRPN_CALLBACK handle_tracker(void *userdata, const vrpn_TRACKERCB t)
{
   
// typedef struct {
//     struct timeval  msg_time;   // Time of the report
//     long        sensor;     // Which sensor is reporting
//     double      pos[3];     // Position of the sensor
//     double      quat[4];    // Orientation of the sensor
// } vrpn_TRACKERCB;

  //printf("handle_tracker\tSensor %d is now at (%g,%g,%g)\n",
  //   t.sensor,
  //   t.pos[0], t.pos[1], t.pos[2]);
 
   if(t.sensor<NUM_SENSORS)
   {
      sensor_received[t.sensor]=true;
      memcpy(send_buffer+t.sensor*64,(unsigned char *)&t.sensor,64); //pack both sensors into same packet
 
      //printf("packet!");
      if(all_sensors_ready())
      {
 
        send_udp(send_buffer,64*NUM_SENSORS);
	clear_all_sensors();

        printf("."); fflush(stdout);

        frames_sent++;
        if(frames_sent==100)
        {
           double elapsed_time=end_timer();
           printf("\nframes per second: %f\n",100.0/elapsed_time);
           start_timer();
           frames_sent=0;
        }
      }
   }
}
//****************************************************************************
//
//   Main function
//
//****************************************************************************

int main(int argc, char *argv[])

{       
	clear_all_sensors();

//	setup_socket("10.236.133.151");//alienware development computer
//	setup_socket("10.190.77.111"); //connected over duke wifi
	setup_socket("10.236.133.63");

	int     done = 0;
        vrpn_Tracker_Remote *tkr;

        // Open the tracker
        //setup_socket("10.236.76.224");

        tkr = new vrpn_Tracker_Remote("Tracker0@localhost");
        // Set up the tracker callback handler

        tkr->register_change_handler(NULL, handle_tracker);

    // the handle_tracker fucntion will be called whenever the
    // tracker position for ANY of the tracker's sensors are updated.
    // if you are interested in only specific sensors (this should be
    // the most common case), use this method instead:

    // tkr->register_change_handler(NULL, handle_tracker,2);
    // handle_tracker will be called only when sensor #2 is updated.


        //
        // main interactive loop
        //
        start_timer();

        while ( ! done ) {
                // Let the tracker do it's thing
                // It will call the callback funtions you registered above
                // as needed

                tkr->mainloop();
        }
}   //main
